package com.zcs.android.app.easy.demo;

public class ProducerConsumerDemo {
    public static void main(String[] args) {
        Buffer buffer = new Buffer();

        new Producer(buffer, 0).start();
        new Consumer(buffer, 0).start();
    }
}
