package com.zcs.android.app.easy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.zcs.android.app.easy.databinding.ActivityMainBinding
import com.zcs.android.app.easy.model.MainModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var mModel: MainModel
    var str: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.inflate(
            layoutInflater, R.layout.activity_main, null, false
        )
        mModel = MainModel().dft()
        binding.model = mModel
        binding.lifecycleOwner = this
        setContentView(binding.root)
        Handler().postDelayed({
            if (str.isNullOrEmpty()) {
                mModel.name.value = "1 你好，Easy Jetpack str is $str"
                tv_welcome.textSize = 32F
            } else {
                mModel.name.value = "2 你好，Easy Jetpack str is $str"
                tv_welcome.textSize = 32F
            }
        }, 2000)
    }
}
