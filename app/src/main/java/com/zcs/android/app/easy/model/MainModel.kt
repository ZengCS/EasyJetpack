package com.zcs.android.app.easy.model

import androidx.lifecycle.MutableLiveData

/**
 * Created by ZengCS on 2021/5/20.
 * E-mail:zengcs@vip.qq.com
 * Add:中国成都
 * Desc:类说明
 */
class MainModel {
    val name = MutableLiveData<String>()
    val age = MutableLiveData<Int>()
    val weight = MutableLiveData<Double>()

    fun dft(): MainModel {
        name.value = "Loading..."
        age.value = 1
        weight.value = 0.toDouble()

        return this
    }
}